<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TimeManagement */

$this->title = 'Create Time Management';
$this->params['breadcrumbs'][] = ['label' => 'Time Managements', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="time-management-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
