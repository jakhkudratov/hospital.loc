<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\WorkingTime */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="working-time-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'begin')->textInput() ?>

    <?= $form->field($model, 'end')->textInput() ?>

    <?= $form->field($model, 'lunch_time_begin')->textInput() ?>

    <?= $form->field($model, 'lounch_time_end')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
