<?php

namespace backend\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "admit".
 *
 * @property int $id
 * @property int|null $doctor_id
 * @property int|null $user_id
 * @property int|null $begin_time
 * @property int|null $end_time
 * @property int|null $line
 * @property int|null $status
 * @property int|null $full_name
 * @property int|null $comment
 * @property string|null $phone
 *
 * @property User $doctor
 * @property User $user
 */
class Admit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['doctor_id', 'user_id', 'begin_time', 'end_time', 'line', 'status', 'full_name', 'comment'], 'integer'],
            [['phone'], 'string', 'max' => 255],
            [['doctor_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['doctor_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'doctor_id' => 'Doctor ID',
            'user_id' => 'User ID',
            'begin_time' => 'Begin Time',
            'end_time' => 'End Time',
            'line' => 'Line',
            'status' => 'Status',
            'full_name' => 'Full Name',
            'comment' => 'Comment',
            'phone' => 'Phone',
        ];
    }

    /**
     * Gets query for [[Doctor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDoctor()
    {
        return $this->hasOne(User::className(), ['id' => 'doctor_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
