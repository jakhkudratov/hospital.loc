<?php

namespace backend\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "time_management".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $week_id
 * @property int|null $working_time_id
 *
 * @property User $user
 * @property Week $week
 * @property WorkingTime $workingTime
 */
class TimeManagement extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'time_management';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'week_id', 'working_time_id'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['working_time_id'], 'exist', 'skipOnError' => true, 'targetClass' => WorkingTime::className(), 'targetAttribute' => ['working_time_id' => 'id']],
            [['week_id'], 'exist', 'skipOnError' => true, 'targetClass' => Week::className(), 'targetAttribute' => ['week' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'week_id' => 'Week ID',
            'working_time_id' => 'Working Time ID',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Gets query for [[WorkingTime]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWorkingTime()
    {
        return $this->hasOne(WorkingTime::className(), ['id' => 'working_time_id']);
    }

    /**
     * Gets query for [[Week]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWeek()
    {
        return $this->hasOne(Week::className(), ['id' => 'week_id']);
    }
}
