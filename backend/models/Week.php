<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "week".
 *
 * @property int|null $id
 * @property string|null $day
 * @property string|null $title
 */
class Week extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'week';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['day', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'day' => 'Day',
            'title' => 'Title',
        ];
    }
}
