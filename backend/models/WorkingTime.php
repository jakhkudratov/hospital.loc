<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "working_time".
 *
 * @property int $id
 * @property int|null $begin
 * @property int|null $end
 * @property int|null $lunch_time_begin
 * @property int|null $lounch_time_end
 *
 * @property TimeManagement[] $timeManagements
 */
class WorkingTime extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'working_time';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['begin', 'end', 'lunch_time_begin', 'lounch_time_end'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'begin' => 'Begin',
            'end' => 'End',
            'lunch_time_begin' => 'Lunch Time Begin',
            'lounch_time_end' => 'Lounch Time End',
        ];
    }

    /**
     * Gets query for [[TimeManagements]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTimeManagements()
    {
        return $this->hasMany(TimeManagement::className(), ['working_time_id' => 'id']);
    }
}
