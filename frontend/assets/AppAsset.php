<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/bootstrap.min.css',
        'css/animate.css',
        'css/colors.css',
        'css/custom.css',
        'css/flaticon.css',
        'css/font-awesome.min.css',
        'css/owl.carousel.css',
        'css/prettyPhoto.css',
        'css/responsive.css',
        'css/versions.css',
    ];
    public $js = [
        'js/all.js',
        'js/custom.js',
        'js/modernizer.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
