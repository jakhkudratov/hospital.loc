<?php
?>

<div id="service" class="services wow fadeIn">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                <div class="inner-services">
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="serv">
                            <span class="icon-service"><img src="uploads/images/service-icon1.png" alt="#" /></span>
                            <h4 class="text-uppercase">Yuqori sifat</h4>
                            <p>Har doim yuqori sifatga intilamiz</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="serv">
                            <span class="icon-service"><img src="uploads/images/service-icon2.png" alt="#" /></span>
                            <h4 class="text-uppercase">Katta Labaratoriyalar</h4>
                            <p>Qulaylik va yuqori texnalogiyalar xizmatingizda.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="serv">
                            <span class="icon-service"><img src="uploads/images/service-icon3.png" alt="#" /></span>
                            <h4 class="text-uppercase">Malakali mutahasislar</h4>
                            <p>Bizda o'z ishini sevadigan va o'z ishidan faxrlanadigan xodimlar ishlashadi.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="serv">
                            <span class="icon-service"><img src="uploads/images/service-icon4.png" alt="#" /></span>
                            <h4 class="text-uppercase">Bolalar markazi</h4>
                            <p>Biz bolalarizgiz uchun ham qayg'uramiz.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="serv">
                            <span class="icon-service"><img src="uploads/images/service-icon5.png" alt="#" /></span>
                            <h4 class="text-uppercase">Ajoyib Infrastruktura</h4>
                            <p>Bizning dam olish va yotoq joylarimiz siz uchun juda qulay.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="serv">
                            <span class="icon-service"><img src="uploads/images/service-icon6.png" alt="#" /></span>
                            <h4 class="text-uppercase">Har doim eng yaxshilari</h4>
                            <p>Biz har doim eng yaxshi xizmatlarni sizga ko'rsatishga harakat qilamiz.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="appointment-form">
                    <h3><span>+</span> Ko'rikka yozilish</h3>
                    <div class="form">
                        <form action="#">
                            <fieldset>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="form-group">
                                            <input type="text" id="name" placeholder="Your Name"  />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="form-group">
                                            <input type="email" placeholder="Email Address" id="email" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 select-section">
                                    <div class="row">
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option>Day</option>
                                                <option>Sunday</option>
                                                <option>Monday</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option>Time</option>
                                                <option>AM</option>
                                                <option>PM</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option>Doctor Name</option>
                                                <option>Mr.XYZ</option>
                                                <option>Mr.ABC</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="form-group">
                                            <textarea rows="4" id="textarea_message" class="form-control" placeholder="Your Message..."></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="center"><button type="submit">Submit</button></div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

