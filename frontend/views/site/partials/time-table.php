<?php
?>
<div id="time-table" class="time-table-section">
    <div class="container">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="row">
                <div class="service-time one" style="background:#2895f1;">
                    <span class="info-icon"><i class="fa fa-ambulance" aria-hidden="true"></i></span>
                    <h3>Tez tibbiy yordam</h3>
                    <p>Biz tezkor tez tibbiy yordam xizmatlarini taklif qilamiz</p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="row">
                <div class="service-time middle" style="background:#0071d1;">
                    <span class="info-icon"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
                    <h3>Ish vaqtlarimiz</h3>
                    <div class="time-table-section">
                        <ul>
                            <li><span class="left">Dushanba - Juma</span><span class="right">8.00 – 18.00</span></li>
                            <li><span class="left">Shanba</span><span class="right">8.00 – 16.00</span></li>
                            <li><span class="left">Yakshanba</span><span class="right">8.00 – 13.00</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="row">
                <div class="service-time three" style="background:#0060b1;">
                    <span class="info-icon"><i class="fa fa-hospital-o" aria-hidden="true"></i></span>
                    <h3>Bizning xizmatlar</h3>
                    <p>Biz oddiy tibbiy ko'rikdan tortib murakkab operatsiyaga qadar yuqori darajadagi xizmatni taqdim etishdan faxrlanamiz.</p>
                </div>
            </div>
        </div>
    </div>
</div>

