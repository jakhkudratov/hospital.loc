<?php
?>
<div id="about" class="section wow fadeIn">
    <div class="container">
        <div class="heading">
            <span class="icon-logo"><img src="uploads/images/icon-logo.png" alt="#"></span>
            <h2>Poliklinika</h2>
        </div>
        <!-- end title -->
        <div class="row">
            <div class="col-md-6">
                <div class="message-box">
                    <h4>Biz nima bilan shug'illanamiz</h4>
                    <h2>Bizning xizmatlar</h2>
                    <p class="lead">Biz oddiy tibbiy ko'rikdan tortib murakkab operatsiyaga qadar yuqori darajadagi xizmatni taqdim etishdan faxrlanamiz.</p>
                    <p> Sizga o'z ishining mutaxasisi bo'lgan doctorlar va hamshiralar xizmat ko'rsatadi.  </p>
                    <a href="#services" data-scroll class="btn btn-light btn-radius btn-brd grd1 effect-1">Learn More</a>
                </div>
                <!-- end messagebox -->
            </div>
            <!-- end col -->
            <div class="col-md-6">
                <div class="post-media wow fadeIn">
                    <img src="uploads/images/about_03.jpg" alt="" class="img-responsive">
                    <a href="http://www.youtube.com/watch?v=nrJtHemSPW4" data-rel="prettyPhoto[gal]" class="playbutton"><i class="flaticon-play-button"></i></a>
                </div>
                <!-- end media -->
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
        <hr class="hr1">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="service-widget">
                    <div class="post-media wow fadeIn">
                        <a href="uploads/images/clinic_01.jpg" data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i class="flaticon-unlink"></i></a>
                        <img src="uploads/images/clinic_01.jpg" alt="" class="img-responsive">
                    </div>
                    <h3>Raqamli boshqaruv sistemasi</h3>
                </div>
                <!-- end service -->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="service-widget">
                    <div class="post-media wow fadeIn">
                        <a href="uploads/images/clinic_02.jpg" data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i class="flaticon-unlink"></i></a>
                        <img src="uploads/images/clinic_02.jpg" alt="" class="img-responsive">
                    </div>
                    <h3>Operatsiya xonalari</h3>
                </div>
                <!-- end service -->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="service-widget">
                    <div class="post-media wow fadeIn">
                        <a href="uploads/images/clinic_03.jpg" data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i class="flaticon-unlink"></i></a>
                        <img src="uploads/images/clinic_03.jpg" alt="" class="img-responsive">
                    </div>
                    <h3>Mutaxassis shifokorlar</h3>
                </div>
                <!-- end service -->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="service-widget">
                    <div class="post-media wow fadeIn">
                        <a href="uploads/images/lab1.jpg" data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i class="flaticon-unlink"></i></a>
                        <img src="uploads/images/lab1.jpg" alt="" class="img-responsive">
                    </div>
                    <h3>Labaratoriyalar</h3>
                </div>
                <!-- end service -->
            </div>
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</div>

