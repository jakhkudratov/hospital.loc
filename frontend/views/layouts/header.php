<?php
?>

<!-- LOADER -->
<div id="preloader">
    <img class="preloader" src="uploads/images/loaders/heart-loading2.gif" alt="">
</div>
<!-- END LOADER -->
<header>
    <div class="header-top wow fadeIn">
        <div class="container">
            <a class="navbar-brand" href="index.html"><img src="uploads/images/logo.png" alt="image"></a>
            <div class="right-header">
                <div class="header-info">
                    <div class="info-inner">
                        <span class="icontop"><img src="uploads/images/phone-icon.png" alt="#"></span>
                        <span class="iconcont"><a href="tel: +998909427076">+99 890 942 70 76</a></span>
                    </div>
                    <div class="info-inner">
                        <span class="icontop"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                        <span class="iconcont"><a data-scroll href="mailto:info@yoursite.com">jakh.kudratov@gmail.com</a></span>
                    </div>
                    <div class="info-inner">
                        <span class="icontop"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
                        <span class="iconcont"><a data-scroll href="#">Daily: 7:00am - 8:00pm</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom wow fadeIn">
        <div class="container">
            <nav class="main-menu">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i class="fa fa-bars" aria-hidden="true"></i></button>
                </div>

                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a class="active" href="/">Asosiy</a></li>
                        <li><a data-scroll href="#about">Biz haqimizda</a></li>
                        <li><a data-scroll href="#service">Xizmatlar</a></li>
                        <li><a data-scroll href="#doctors">Doctorlar</a></li>
                        <li><a data-scroll href="#price">Narxlar</a></li>
                        <li><a data-scroll href="#testimonials">Guvohnomalar</a></li>
                        <li><a data-scroll href="#getintouch">Aloqa</a></li>
                    </ul>
                </div>
            </nav>
            <div class="serch-bar">
                <div id="custom-search-input">
                    <div class="input-group col-md-12">
                        <input type="text" class="form-control input-lg" placeholder="Search" />
                        <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="button">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        </button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>


