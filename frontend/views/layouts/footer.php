<?php
?>

<footer id="footer" class="footer-area wow fadeIn">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="logo padding">
                    <a href=""><img src="uploads/images/logo.png" alt=""></a>
                    <p>Biz doim eng yuqori darajali xizmatlarni taqdim etamiz.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="footer-info padding">
                    <h3>Biz bilan bog'lanish</h3>
                    <p><i class="fa fa-map-marker" aria-hidden="true"></i> Toshkent shahar Amir Temur ko'chasi 129-uy</p>
                    <p><i class="fa fa-paper-plane" aria-hidden="true"></i> jakh.kudratov@gmail.com</p>
                    <p><i class="fa fa-phone" aria-hidden="true"></i> +99 890 942 70 76</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="subcriber-info">
                    <h3>Hoziroq obuna bo'ling</h3>
                    <p>Mutaxassislarimizdan sog'lom yangiliklar, maslahatlar va muammolarga echimlar oling.</p>
                    <div class="subcriber-box">
                        <form id="mc-form" class="mc-form">
                            <div class="newsletter-form">
                                <input type="email" autocomplete="off" id="mc-email" placeholder="Email address" class="form-control" name="EMAIL">
                                <button class="mc-submit" type="submit"><i class="fa fa-paper-plane"></i></button>
                                <div class="clearfix"></div>
                                <!-- mailchimp-alerts Start -->
                                <div class="mailchimp-alerts">
                                    <div class="mailchimp-submitting"></div>
                                    <!-- mailchimp-submitting end -->
                                    <div class="mailchimp-success"></div>
                                    <!-- mailchimp-success end -->
                                    <div class="mailchimp-error"></div>
                                    <!-- mailchimp-error end -->
                                </div>
                                <!-- mailchimp-alerts end -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="copyright-area wow fadeIn">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="footer-text">
                    <p>© 2021 Lifecare. All Rights Reserved.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="social">
                    <ul class="social-links">
                        <li><a href="/"><i class="fa fa-rss"></i></a></li>
                        <li><a href="/"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="/"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="/"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="/"><i class="fa fa-youtube"></i></a></li>
                        <li><a href="/"><i class="fa fa-pinterest"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end copyrights -->
<a href="/" data-scroll class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>
