<?php

use yii\db\Migration;

/**
 * Class m210505_092645_main_tables_two
 */
class m210505_092645_main_tables_two extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\common\models\User::tableName(), 'role', $this->integer());
        $this->addColumn(\common\models\User::tableName(), 'first_name', $this->integer());
        $this->addColumn(\common\models\User::tableName(), 'last_name', $this->integer());
        $this->addColumn(\common\models\User::tableName(), 'section_id', $this->integer());
        $this->addColumn(\common\models\User::tableName(), 'room_id', $this->integer());

        $this->createIndex(
            'idx-user-section_id',
            'user',
            'section_id'
        );

        $this->addForeignKey(
            'fk-user-section_id',
            'user',
            'section_id',
            'section',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-user-room_id',
            'user',
            'room_id'
        );

        $this->addForeignKey(
            'fk-user-room_id',
            'user',
            'room_id',
            'room',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210505_092645_main_tables_two cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210505_092645_main_tables_two cannot be reverted.\n";

        return false;
    }
    */
}
