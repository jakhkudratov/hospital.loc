<?php

use yii\db\Migration;

/**
 * Class m210505_102553_creatae_modify_week_table
 */
class m210505_102553_creatae_modify_week_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn(\backend\models\Week::tableName(), 'id', $this->primaryKey());

        $this->addForeignKey(
            'fk-time_management-week_id',
            'time_management',
            'week_id',
            'week',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210505_102553_creatae_modify_week_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210505_102553_creatae_modify_week_table cannot be reverted.\n";

        return false;
    }
    */
}
