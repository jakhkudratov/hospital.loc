<?php

use yii\db\Migration;

/**
 * Class m210505_072415_create_main_tables
 */
class m210505_072415_create_main_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('section', [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ]);
        $this->createTable('room', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'number' => $this->integer(),
            'section_id' => $this->integer()
        ]);

        $this->createIndex(
            'idx-room-section_id',
            'room',
            'section_id'
        );

        $this->addForeignKey(
            'fk-room-section_id',
            'room',
            'section_id',
            'section',
            'id',
            'CASCADE'
        );

        $this->createTable('position', [
            'id' => $this->primaryKey(),
            'title' => $this->string()
        ]);

        $this->createTable('week', [
            'id' => $this->integer(),
            'day' => $this->string(),
            'title' => $this->string()
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210505_072415_create_main_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210505_072415_create_main_tables cannot be reverted.\n";

        return false;
    }
    */
}
