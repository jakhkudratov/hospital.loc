<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%admit}}`.
 */
class m210505_095713_create_admit_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\common\models\User::tableName(), 'phone', $this->string());

        $this->createTable('{{%admit}}', [
            'id' => $this->primaryKey(),
            'doctor_id' => $this->integer(),
            'user_id' => $this->integer(),
            'begin_time' => $this->integer(),
            'end_time' => $this->integer(),
            'line' => $this->integer(),
            'status' => $this->integer(),
            'full_name' => $this->integer(),
            'comment' => $this->integer(),
            'phone' => $this->string()
        ]);


        $this->createIndex(
            'idx-admit-user_id',
            'admit',
            'user_id'
        );

        $this->addForeignKey(
            'fk-admit-user_id',
            'admit',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
        $this->createIndex(
            'idx-admit-doctor_id',
            'admit',
            'doctor_id'
        );

        $this->addForeignKey(
            'fk-admit-doctor_id',
            'admit',
            'doctor_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%admit}}');
    }
}
