<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%working_time}}`.
 */
class m210505_094057_create_working_time_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%working_time}}', [
            'id' => $this->primaryKey(),
            'begin' => $this->integer(),
            'end' => $this->integer(),
            'lunch_time_begin' => $this->integer(),
            'lounch_time_end' => $this->integer()
        ]);

        $this->createTable('time_management', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'week_id' => $this->integer(),
            'working_time_id' => $this->integer()
        ]);

        $this->createIndex(
            'idx-time_management-user_id',
            'time_management',
            'user_id'
        );

        $this->addForeignKey(
            'fk-time_management-user_id',
            'time_management',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
        $this->createIndex(
            'idx-time_management-week_id',
            'time_management',
            'week_id'
        );

        $this->createIndex(
            'idx-time_management-working_time_id',
            'time_management',
            'working_time_id'
        );

        $this->addForeignKey(
            'fk-time_management-working_time_id',
            'time_management',
            'working_time_id',
            'working_time',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%working_time}}');
    }
}
